<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Entity\Room;
use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }
    public function load(ObjectManager $manager)
    {

        $faker = Factory::create('fr_FR');

        $user = new User();
        $user->setEmail('admin@test.fr');
        $user->setRoles(["ROLE_USER","ROLE_ADMIN"]);
        $user->setPassword($this->encoder->encodePassword($user,"Admin"));

        $manager->persist($user);

        for($i=0;$i<101;$i++){
            $room = new Room();

            $room->setUser($user);
            $room->setName('Salle n°'.$i);
            $room->setCapacity(rand(3,20));
            $room->setCity($faker->city);
            $room->setDescription($faker->realText(250));
            $room->setIsAvailable(rand(0,1));
            $room->setCreatedAt(new \DateTime());

            $manager->persist($room);
        }

        for($e=0;$e<11;$e++){
            $category = new Category();
            $category->setName($faker->name);

            $manager->persist($category);
        }

        $manager->flush();
    }
}